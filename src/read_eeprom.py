import argparse
import csv
from pydoc import locate
import sys
import struct

class file_parser:

    def __init__(self):
        self.archi_binary=[]
        
        self.conv_type_tab={"BOOLEAN" : '?',\
                            "UINT8" : 'B',\
                            "INT8" : 'b',\
                            "UINT16":'H',\
                            "INT16":'h',\
                            "UINT32":'I',\
                            "INT32":'i',\
                            "UINT64" :'Q',\
                            "INT64" :'q',\
                            "FLOAT64" :'f',\
                            "FLOAT32" :'f'}
        
    def conv_type(self,row,data):    
        if row['TYPE'].upper() == "STRING" :
            return struct.unpack('<'+str(len(data))+'s',data)[0].decode('ASCII').rstrip("0")

        return struct.unpack('<'+self.conv_type_tab[row['TYPE'].upper()],data)[0]
        
    def size_type(self,row):
        if row['TYPE'].upper() == "STRING" :
            return int(row['MAX_LENGTH']);
        if row['TYPE'].upper() == "BOOLEAN":
            return 1
        if row['TYPE'].upper() == "UINT8" or \
           row['TYPE'].upper() == "INT8":
            return 1
        if row['TYPE'].upper() == "UINT16" or \
           row['TYPE'].upper() == "INT16" :
            return 2
        if row['TYPE'].upper() == "UINT32" or \
           row['TYPE'].upper() == "INT32" or \
           row['TYPE'].upper() == "FLOAT32" or \
           row['TYPE'].upper() == "FLOAT64": 
            return 4      
        if row['TYPE'].upper() == "UINT64" or \
           row['TYPE'].upper() == "INT64":
            return 8  
            
    def open_param(self,file_setting):
        with open(file_setting) as csvfile:
            datas = csv.DictReader(csvfile, delimiter=';')    
            for row in datas : 
                tmp = {}
                tmp['NAME'] = row['NAME']
                tmp['GROUP'] = row['GROUP']
                tmp['TYPE'] = row['TYPE']
                tmp['MAX_LENGTH'] = row['MAX_LENGTH']
                tmp['VALUE'] = row['VALUE']
                self.archi_binary.append(tmp) 
    
    def run(self, file_setting, file_binary):       
        self.open_param(file_setting)
        
        if file_binary :
            with open(file_binary,'rb') as file:
                file.seek(0)
                extract_value = file.read()
                                
                index = 0
                for row in self.archi_binary :
                
                    index_final = index + self.size_type(row) 
                    data = extract_value[index:index_final]
                                   
                    data_tr = self.conv_type(row,data)
                   
                    addr = index 
                    addr_final = index_final -1
                    print("addr : "+ str(hex(addr)) +" addr_final : "+ str(hex(addr_final)))
                    print("raw : " +  data.hex().rstrip("0"))
                    print("name : " + row['NAME'] +" group : " +row['GROUP'] +" data : "+ str(data_tr) + "\n")
                    
                    index = index_final
            


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("file_setting", help="we need a csv file")
    parser.add_argument("file_binary",help="name of binary to read")
    args = parser.parse_args()
    
    fileParser = file_parser()
    fileParser.run(args.file_setting, args.file_binary)

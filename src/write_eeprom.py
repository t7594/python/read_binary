import argparse
import csv
from pydoc import locate
import sys
import struct

class file_parser:

    def __init__(self):
        self.archi_binary=[]
        
        self.conv_type_tab={"BOOLEAN" : '?',\
                            "UINT8" : 'B',\
                            "INT8" : 'b',\
                            "UINT16":'H',\
                            "INT16":'h',\
                            "UINT32":'I',\
                            "INT32":'i',\
                            "UINT64" :'Q',\
                            "INT64" :'q',\
                            "FLOAT64" :'f',\
                            "FLOAT32" :'f'}
        
    def conv_type(self,row,data):    
        if row['TYPE'].upper() == "STRING" :
            return struct.pack('<'+row['MAX_LENGTH']+'s',data.encode('ASCII'))

        if (row['TYPE'].upper() == "FLOAT64") or (row['TYPE'].upper() == "FLOAT32") :
            return struct.pack('<'+self.conv_type_tab[row['TYPE'].upper()],float(data))

        return struct.pack('<'+self.conv_type_tab[row['TYPE'].upper()],int(data))
            
    def open_param(self,file_setting):
        with open(file_setting) as csvfile:
            datas = csv.DictReader(csvfile, delimiter=';')    
            for row in datas : 
                tmp = {}
                tmp['NAME'] = row['NAME']
                tmp['GROUP'] = row['GROUP']
                tmp['TYPE'] = row['TYPE']
                tmp['MAX_LENGTH'] = row['MAX_LENGTH']
                tmp['VALUE'] = row['VALUE']
                self.archi_binary.append(tmp) 

    def run(self, file_setting, file_binary):       
        self.open_param(file_setting)
        
        if file_binary :
            with open(file_binary,'wb') as file:
                file.seek(0)
                                
                index = 0
                for row in self.archi_binary :
                                                   
                    data_tr = self.conv_type(row,row['VALUE'])

                    file.write(data_tr)
                   

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("file_setting", help="we need a csv file")
    parser.add_argument("file_binary",help="name of binary to write")
    args = parser.parse_args()
    
    fileParser = file_parser()
    fileParser.run(args.file_setting, args.file_binary)

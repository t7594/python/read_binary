import argparse

class file_diff:
    def run(self, eeprom1, eeprom2, feek):
        
        with open(eeprom1) as file:
            with open(eeprom2) as file2:
                file.seek(feek)
                file2.seek(feek)
                
                data = file.read(1)
                data1 = file2.read(1)
                
                while data and data1 :
                    if(data != data1):
                        print str(hex(file.tell()))[:-1] +" : eeprom1 = " + data.encode("hex") +" / eeprom2 = " + data1.encode("hex")
                    data = file.read(1)
                    data1 = file2.read(1)
                
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("eeprom1", help="we need first eeprom")
    parser.add_argument("eeprom2", help="we need second eeprom")
    parser.add_argument("-f","--feek",help="adress start in decimal", type=int, default =0 )
    args = parser.parse_args()
    
    fileDiff = file_diff()
    fileDiff.run(args.eeprom1, args.eeprom2, args.feek)

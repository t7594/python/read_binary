# Read binary in linux or with python

## Read a binary file with linux

For read a binary file, we need to use :
    hexdump "file_name"

> :warning: **this command by default use the pc endian**
